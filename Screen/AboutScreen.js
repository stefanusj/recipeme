import React, { Component } from "react";
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  Linking,
  TextInput,
  ScrollView,
  TouchableOpacity,
  View
} from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import IngredientItem from "./IngredientItem";

export default class AboutScreen extends Component {
  loadInBrowser = url => {
    Linking.openURL(url).catch(err => console.error("Couldn't load page", err));
  };

  render() {
    return (
      <View style={styles.container}>
        <FontAwesome
          name="close"
          size={32}
          style={styles.back}
          onPress={() => this.props.navigation.pop()}
        />
        <Image
          source={require("../assets/me.jpg")}
          style={styles.profilePicture}
        />
        <Text style={styles.title}>Stefanus Julianto</Text>
        <Text style={styles.caption}>One Step Back, Two Steps Forward</Text>
        <View style={styles.containerAbout}>
          <Text style={styles.subtitle}>About Me</Text>
          <View style={styles.containerSocial}>
            <TouchableOpacity
              onPress={() => this.loadInBrowser("https://gitlab.com/stefanusj")}
            >
              <View
                style={{ ...styles.socialButton, backgroundColor: "#FD7E14" }}
              >
                <FontAwesome name="gitlab" size={32} color="white" />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.loadInBrowser("https://github.com/stefanusj")}
            >
              <View
                style={{ ...styles.socialButton, backgroundColor: "#24292E" }}
              >
                <FontAwesome name="github" size={32} color="white" />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.loadInBrowser("https://stefanusj.com")}
            >
              <View
                style={{ ...styles.socialButton, backgroundColor: "#2F80ED" }}
              >
                <FontAwesome name="user" size={32} color="white" />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end"
  },
  back: {
    position: "absolute",
    padding: 16,
    left: 0,
    top: 0,
    zIndex: 999
  },
  profilePicture: {
    position: "absolute",
    width: "100%",
    height: "100%",
    zIndex: 0
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "white",
    padding: 8,
    alignSelf: "center",
    backgroundColor: "rgba(0, 0, 0, 0.1)"
  },
  caption: {
    fontSize: 18,
    padding: 8,
    marginBottom: 8,
    color: "white",
    fontStyle: "italic",
    alignSelf: "center",
    backgroundColor: "rgba(0, 0, 0, 0.1)"
  },
  containerAbout: {
    paddingTop: 8,
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
    backgroundColor: "white"
  },
  subtitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 8,
    padding: 16,
    alignSelf: "center"
  },
  containerSocial: {
    flexDirection: "row",
    justifyContent: "center"
  },
  socialButton: {
    marginBottom: 16,
    marginHorizontal: 16,
    width: 68,
    height: 68,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "black"
  }
});
